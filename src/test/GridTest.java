package test;

import model.Direction;
import model.Grid;
import model.Tile;
import model.Wall;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class GridTest {

    private Grid testGrid;

    @BeforeEach
    void setUp() {
        testGrid = new Grid();
    }

    @Test
    void testCollide() {
        Tile[][] tiles = testGrid.getTiles();
        Tile[] tileArray = tiles[0];
        tileArray[0].setNumber(2);
        tileArray[2].setNumber(4);
        tileArray[0].collide(tileArray[0], Direction.RIGHT);
        Tile[] x = {new Tile(), new Tile(), new Tile(), new Tile()};

        x[0].giveNeighbor(new Wall(), Direction.LEFT);
        x[0].giveNeighbor(x[1], Direction.RIGHT);
        x[1].giveNeighbor(x[0], Direction.LEFT);
        x[1].giveNeighbor(x[2], Direction.RIGHT);
        x[2].giveNeighbor(x[1], Direction.LEFT);
        x[2].giveNeighbor(x[3], Direction.RIGHT);
        x[3].giveNeighbor(x[2], Direction.LEFT);
        x[3].giveNeighbor(new Wall(), Direction.RIGHT);


        x[3].setNumber(4);
        x[2].setNumber(2);
        assertEquals(x[0].getNumber(), tileArray[0].getNumber());
        assertEquals(x[1].getNumber(), tileArray[1].getNumber());
        assertEquals(x[2].getNumber(), tileArray[2].getNumber());
        assertEquals(x[3].getNumber(), tileArray[3].getNumber());

    }

}