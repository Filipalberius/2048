package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import static model.Direction.*;

public class Grid {

    private Tile[][] tiles;

    public Tile[][] getTiles() {
        return tiles;
    }

    public Grid(){
        tiles = new Tile[4][4];
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                tiles[i][j] = new Tile();
            }
        }
        giveNeighbors();
    }

    private void giveNeighbors(){
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 4; j++){
                Tile current = tiles[i][j];
                if(i == 0){
                    current.giveNeighbor(new Wall(), UP);
                } else {
                    current.giveNeighbor(tiles[i-1][j], UP);
                }

                if(i == 3){
                    current.giveNeighbor(new Wall(), DOWN);
                } else {
                    current.giveNeighbor(tiles[i+1][j], DOWN);
                }

                if(j == 0){
                    current.giveNeighbor(new Wall(), LEFT);
                } else {
                    current.giveNeighbor(tiles[i][j-1], LEFT);
                }

                if(j == 3){
                    current.giveNeighbor(new Wall(), RIGHT);
                } else {
                    current.giveNeighbor(tiles[i][j+1], RIGHT);
                }
            }
        }
    }

    void addSlot() throws IllegalArgumentException {
        Random random = new Random();
        List<Tile> contenders = emptySlots();
        if (contenders.isEmpty()){
            throw new IllegalArgumentException();
        }
        int rng = random.nextInt(10) + 1;
        if(rng == 10){
            contenders.get(random.nextInt(contenders.size())).setNumber(4);
        } else {
            contenders.get(random.nextInt(contenders.size())).setNumber(2);
        }

    }

    private List<Tile> emptySlots(){
        List<Tile> tmp = new ArrayList<>();
        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 4; j++) {
                if(tiles[i][j].getNumber() == 0){
                    tmp.add(tiles[i][j]);
                }
            }
        }
        return tmp;
    }

    void makeMove(Direction dir){
        switch(dir){
            case LEFT:
                for(int i = 3; i > 0; i--){
                    tiles[i][2].collide(tiles[i][3].getLeft(), dir);
                }
            case DOWN:
                for(int j = 0; j < 4; j++){
                    tiles[1][j].collide(tiles[0][j].getDown(), dir);
                }
                break;
            case RIGHT:
                for(int i = 0; i < 4; i++){
                    tiles[i][1].collide(tiles[i][0].getRight(), dir);
                }
                break;
            case UP:
                for(int j = 3; j > 0; j--){
                    tiles[2][j].collide(tiles[3][j].getUp(), dir);
                }
                break;
        }
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(Tile[] x: tiles){
            sb.append("\n_________________________________\n");
            sb.append("\n|");
            for(Tile y : x) {
                sb.append(y.toString());
                sb.append("|");
            }
        }
        sb.append("\n_________________________________");
        return sb.toString();
    }

    void print(){
        System.out.println(toString());
    }
}
