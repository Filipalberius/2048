package model;

/**
 * Represents one slot in the Game's grid. Contains a number.
 */
public class Tile extends Position {

    private int number = 0;
    private Position left;
    private Position down;
    private Position right;
    private Position up;

    public Tile(){
        super(true);
    }

    public void giveNeighbor(Position pos, Direction dir){
        switch(dir){
            case LEFT: left = pos;
                break;

            case DOWN: down = pos;
                break;

            case RIGHT: right = pos;
                break;

            case UP: up = pos;
                break;
        }
    }

    public int getNumber(){
        return number;
    }

    public void setNumber(int num){
        this.number = num;
    }

    public Position getDown() {
        return down;
    }

    public Position getLeft() {
        return left;
    }

    public Position getRight() {
        return right;
    }

    public Position getUp() {
        return up;
    }

    @Override
    public void add(Position other){
        if(other instanceof Tile) {
            this.number += ((Tile)other).number;
        }
    }

    @Override
    public boolean collide(Position pos, Direction dir) {

        if(next(dir).collide(this, dir)) {
            if (number == previous(dir).getNumber()) {
                number += previous(dir).getNumber();
                ((Tile) previous(dir)).setNumber(0);
            } else if (previous(dir).getNumber() != 0 && number == 0) {
                number = previous(dir).getNumber();
                ((Tile) previous(dir)).setNumber(0);
            } else {

            }
        }

        return true;
    }

    private Position next(Direction dir) {

        switch (dir) {
            case LEFT:
                return left;

            case DOWN:
                return down;

            case RIGHT:
                return right;

            case UP:
                return up;

        }

        return new Wall();
    }

    private Position previous(Direction dir){

        switch (dir.opDir()) {
            case LEFT:
                return left;

            case DOWN:
                return down;

            case RIGHT:
                return right;

            case UP:
                return up;

        }

        return new Wall();
    }



    public String toString(){

        int length = String.valueOf(number).length();

        if(number == 0){
            return"       ";
        }

        switch (length) {
            case 1:
                return ("   " + number + "   ");

            case 2:
                return ("   " + number + "  ");

            case 3:
                return ("  " + number + "  ");

            case 4:
                return ("  " + number + " ");

            default:
                return (" " + number + " ");

        }
    }
}
