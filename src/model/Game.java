package model;

import java.util.Scanner;

public class Game {

    private Grid grid;
    private DirectionFactory moveTypeFactory;

    public Game(){
        this.grid = new Grid();
        this.moveTypeFactory = new DirectionFactory();
        grid.addSlot();
        grid.addSlot();
    }

    private void giveTips(){
        StringBuilder sb = new StringBuilder();
        sb.append("Please make a move!\n");
        sb.append("Press either 'u' to shift the tiles upward,\n");
        sb.append("'d' to shift the tiles down,\n");
        sb.append("'l' to shift the tiles left,\n");
        sb.append("or 'r' to shift the tiles right!");
        System.out.println(sb.toString());
    }

    private Direction promptMove(){
        System.out.println("make a move: ");
        Scanner input = new Scanner(System.in);
        String move = input.next();
        try {
            moveTypeFactory.build(move);
        } catch (IllegalArgumentException e){
            System.out.println("That is not a move");
            giveTips();
            promptMove();
        }
        return moveTypeFactory.build(move);
    }

    private void run(){
        //TODO fixa så att detta funkar
        //TODO fixa så att den märker GAMEOVER & WIN
        boolean gameOver = false;
        boolean hasWon = false;
        grid.print();
        grid.makeMove(promptMove());
        try {
            grid.addSlot();
        } catch (IllegalArgumentException e){
            gameOver = true;
        }

        if(!gameOver && !hasWon){
            run();
        }
    }

    public static void main(String[] args){
        Game game = new Game();
        game.run();

    }
}