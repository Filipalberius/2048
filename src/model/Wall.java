package model;

public class Wall extends Position {


    public Wall() {
        super(false);
    }

    @Override
    public boolean collide(Position pos, Direction dir) {
        return false;
    }

    @Override
    public void add(Position other) throws IllegalArgumentException {
        throw new IllegalArgumentException();
    }

    @Override
    public int getNumber() throws IllegalArgumentException {
        throw new IllegalArgumentException();
    }

    @Override
    public Position getDown() {
        return null;
    }

    @Override
    public Position getLeft() {
        return null;
    }

    @Override
    public Position getRight() {
        return null;
    }

    @Override
    public Position getUp() {
        return null;
    }
}
