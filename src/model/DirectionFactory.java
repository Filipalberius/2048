package model;

import java.lang.IllegalArgumentException;

import static model.Direction.*;

public class DirectionFactory {

    public Direction build(String type) throws IllegalArgumentException {
        switch (type) {
            case "l":
                return LEFT;
            case "d":
                return DOWN;
            case "r":
                return RIGHT;
            case "u":
                return UP;
            default:
                throw new IllegalArgumentException("no such move");
        }
    }
}
