package model;

abstract class Position {

    private boolean isTile;

    abstract boolean collide(Position pos, Direction dir);

    abstract void add(Position other) throws IllegalArgumentException;

    abstract int getNumber();

    abstract Position getDown();

    abstract Position getLeft();

    abstract Position getRight();

    abstract Position getUp();

    Position(boolean isTile){
        this.isTile = isTile;
    }
}


